const Categoria = require("../models/Categoria");
// CRUD Create  Crear
exports.crearCategoria = async (req,res) => {

    try{
        let categoria;
         // creamos nuestra categoria
         categoria = new Categoria(req.body);
         await categoria.save();
         res.send(categoria);

}catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos en crear Categoria linea 14");
}
}
// CRUD Read  Leer, mostrar
exports.mostrarCategorias =async (req,res) =>{

try{
   const categorias = await Categoria.find();
  res.json(categorias)

} catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos en mostrar categorias linea 26");
}
}
// CRUD Update  Obtener, traer = GET
exports.obtenerCategoria = async (req,res) =>{
 try{
let categoria = await Categoria.findById(req.params.id);
if (!categoria){
    res.status(404).json({msg: 'la categoria no existe'})
}
res.json(categoria);

 }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos en obtener categorias POR Id linea 40");

}
}
// CRUD Delete Borrar, eliminar
exports.eliminarCategoria = async (req,res) =>{
    try{
        let categoria = await Categoria.findById(req.params.id);
        if (!categoria){
            res.status(404).json({msg: 'la categoria no existe'})
        }
        await Categoria.findByIdAndRemove({ _id: req.params.id})
        res.json({msg: 'categoria eliminada con exito'});
}catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos en eliminar categorias linea 55");
}
}

exports.actualizarCategoria = async (req,res) =>{
    try{
     const {nombreCategoria, descripcionCategoria} = req.body;
     let categoria = await Categoria.findById(req.params.id); 
     if (!categoria){
        res.status(404).json({msg: 'la categoria no existe'})
    }
    categoria.nombreCategoria = nombreCategoria;
    categoria.descripcionCategoria = descripcionCategoria;


    categoria = await Categoria.findOneAndUpdate({_id: req.params.id}, categoria, {new:true})
    res.json(categoria);

    }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos en actualizar categorias linea 75");
}
}
